<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // creacion de la tabla capital con los campos id y name  y id relacion con la table department  
    public function up()
    {
        Schema::create('capital', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('id_department')->unsigned();

            
            $table->foreign('id_department')->references('id')->on('department');
        });
    }

    /**
     * Reverse the migrations.    DataBase db_laravel_country
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capital');
    }
}
