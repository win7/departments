<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// el modelo department permite el acceso a la tabla department de la base de datos
class Department extends Model
{
	// comprobacion de la tabla department de la base de datos
    protected $table ="department";
}