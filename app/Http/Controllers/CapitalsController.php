<?php

namespace App\Http\Controllers;
use App\Capital;
use Illuminate\Http\Request;

class CapitalsController extends Controller
{
    public function index() 
    {
    	// contructor de consultas para llamar todos los datos de la tabla capital
    	$capitals = Capital::all();
        //se enlaza la vista index del el directorio capitals para listar las capitales
    	return view('capitals.index', compact('capitals'));
    }

    // 
    public function show($id) 
    {
        // contructor de consultas para encontrar la capital por in identificador unico
        $capital = Capital::find($id);
        //se enlaza la vista show del el directorio capitals para listar el departamento
    	return view('capitals.show', compact('capital'));
    }
}