<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// el modelo capital permite el acceso a la tabla capital de la base de datos
class Capital extends Model
{
	// comprobacion de la tabla capital de la base de datos
    protected $table = "capital";

    // relacion de la tabla capital con la tabla departament por su id
    public function department() {
    	return $this->belongsTo(Department::class, 'id_department');
    }
}