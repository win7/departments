<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Configuracion de la ruta de inicio principal y referencia del controlador con el metodo index*/
Route::get('/', 'CapitalsController@index');
/*Configuracion de la ruta de consulta  y referencia del controlador con el metodo show*/
Route::get('/capitals/{id}', 'CapitalsController@show');

