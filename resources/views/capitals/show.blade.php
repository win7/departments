<!DOCTYPE html>
<html>
<head>
	<title>Department</title>
	<style>
	/*diseño del contenedor principal departments*/
	.content {
		position: absolute;
		box-sizing: border-box;
		display: inline-block;
		background: whitesmoke;
		top: 39.1%;
		left: 50%;
		transform: translate(-50%, -50%);
		font-family: verdana;
		width: 20em;
		border-radius: 1em;
		box-shadow: 1px 1px 4px  black;
	}
	/*diseño del nombre del deartamento*/
	.content ol {
		font-size: 2em;
	}
	.content ol li a {
        text-decoration: none;
        font-size: 2em;
        color: black;
	}
	/*diseño del boton retroceso*/
	.return  {
		background-color: rgb(0, 0, 0, 0.5);
		padding: 1em;
		border-bottom-right-radius: .5em;
        border-bottom-left-radius: .5em;
        text-decoration: none;
        display: block;
        color: white;
        text-align: center;
        font-size: 1.5em;
	}
	.return:hover {
		background-color: rgb(0, 0, 0, 0.8);
		color: white;
	}
	h1 {
		text-align: center;
	}
	p  {
		text-align: center;
		font-size: 2em;
	}
</style>
</head>
<body>
	<div class="content">
        <h1>Department</h1>
        <!--se muestra el nombre del departamento relacionado con la ciudad-->
        <p>{{ $capital->department->name }}</p> 	
        <a class="return" href="{{ URL::previous() }}">Capitals</a>
	</div>
</body>
</html>