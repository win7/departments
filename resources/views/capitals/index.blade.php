<!DOCTYPE html>
<html>
<head>
    <title>Capitals</title>
	<style>
	/*diseño del contenedor principal capitals*/
	.content {
		position: absolute;
		box-sizing: border-box;
		display: inline-block;
		background: whitesmoke;
		top: 40%;
		left: 50%;
		transform: translate(-50%, -50%);
		font-family: verdana;
		width: 20em;
		border-radius: 1em;
		box-shadow: 1px 1px 4px  black;
		text-align: center;
	}
	/*diseño de las capitales traidas de la base de datos*/
	.content ol li {
        font-size: 2em;
	}
	.content ol li a {
        text-decoration: none;
        color: black;
	}

	h1 {
		text-align: center;
	}
</style>
</head>
<body>
	<div class="content">
	    <h1>Capitals</h1>
        <ol>
        	<!--muostrando cada una de las capitales traidas de la base de datos-->
  	          @foreach ($capitals as $capital)
  	              <li><a href="/capitals/{{ $capital->id }}">{{$capital->name}}</a></li>
  	          @endforeach
        </ul>
	</div>

</body>
</html>